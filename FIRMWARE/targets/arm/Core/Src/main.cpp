//
// Created by Andrzej on 15.12.2021.
//
#include <array>
#include <cstdio>
#include <string>
#include "main.h"


extern UART_HandleTypeDef huart2;
const std::array <std::pair<GPIO_TypeDef*, uint16_t>, 36> SENSORS{
        std::make_pair(SENS12_MOSI_GPIO_Port, SENS12_MOSI_Pin),
        std::make_pair(SENS12_MISO_GPIO_Port, SENS12_MISO_Pin),
        std::make_pair(SENS12_SCK_GPIO_Port, SENS12_SCK_Pin),
        std::make_pair(SENS1_CS_GPIO_Port, SENS1_CS_Pin),
        std::make_pair(SENS1_RST_GPIO_Port, SENS1_RST_Pin),
        std::make_pair(SENS1_ANALOG_GPIO_Port, SENS1_ANALOG_Pin),
        std::make_pair(SENS1_PWM_GPIO_Port, SENS1_PWM_Pin),
        std::make_pair(SENS1_INT_GPIO_Port, SENS1_INT_Pin),
        std::make_pair(SENS1_RX_GPIO_Port, SENS1_RX_Pin),
        std::make_pair(SENS1_TX_GPIO_Port, SENS1_TX_Pin),
        std::make_pair(I2C2_SCL_GPIO_Port, I2C2_SCL_Pin),
        std::make_pair(I2C2_SDA_GPIO_Port, I2C2_SDA_Pin),
        std::make_pair(SENS2_CS_GPIO_Port, SENS2_CS_Pin),
        std::make_pair(SENS2_RST_GPIO_Port, SENS2_RST_Pin),
        std::make_pair(SENS2_ANALOG_GPIO_Port, SENS2_ANALOG_Pin),
        std::make_pair(SENS2_PWM_GPIO_Port, SENS2_PWM_Pin),
        std::make_pair(SENS2_INT_GPIO_Port, SENS2_INT_Pin),
        std::make_pair(SENS2_RX_GPIO_Port, SENS2_RX_Pin),
        std::make_pair(SENS2_TX_GPIO_Port, SENS2_TX_Pin),
        std::make_pair(SENS34_MOSI_GPIO_Port, SENS34_MOSI_Pin),
        std::make_pair(SENS34_MISO_GPIO_Port, SENS34_MISO_Pin),
        std::make_pair(SENS34_SCK_GPIO_Port, SENS34_SCK_Pin),
        std::make_pair(SENS3_CS_GPIO_Port, SENS3_CS_Pin),
        std::make_pair(SENS3_RST_GPIO_Port, SENS3_RST_Pin),
        std::make_pair(SENS3_ANALOG_GPIO_Port, SENS3_ANALOG_Pin),
        std::make_pair(SENS3_PWM_GPIO_Port, SENS3_PWM_Pin),
        std::make_pair(SENS3_INT_GPIO_Port, SENS3_INT_Pin),
        std::make_pair(SENS3_RX_GPIO_Port, SENS3_RX_Pin),
        std::make_pair(SENS3_TX_GPIO_Port, SENS3_TX_Pin),
        std::make_pair(SENS4_CS_GPIO_Port, SENS4_CS_Pin),
        std::make_pair(SENS4_RST_GPIO_Port, SENS4_RST_Pin),
        std::make_pair(SENS4_ANALOG_GPIO_Port, SENS4_ANALOG_Pin),
        std::make_pair(SENS4_PWM_GPIO_Port, SENS4_PWM_Pin),
        std::make_pair(SENS4_INT_GPIO_Port, SENS4_INT_Pin),
        std::make_pair(SENS4_RX_GPIO_Port, SENS4_RX_Pin),
        std::make_pair(SENS4_TX_GPIO_Port, SENS4_TX_Pin)
};


const std::array <std::pair<GPIO_TypeDef*, uint16_t>, 12> SENSOR1  {
        std::make_pair(SENS12_MOSI_GPIO_Port, SENS12_MOSI_Pin),
        std::make_pair(SENS12_MISO_GPIO_Port, SENS12_MISO_Pin),
        std::make_pair(SENS12_SCK_GPIO_Port, SENS12_SCK_Pin),
        std::make_pair(SENS1_CS_GPIO_Port, SENS1_CS_Pin),
        std::make_pair(SENS1_RST_GPIO_Port, SENS1_RST_Pin),
        std::make_pair(SENS1_ANALOG_GPIO_Port, SENS1_ANALOG_Pin),
        std::make_pair(SENS1_PWM_GPIO_Port, SENS1_PWM_Pin),
        std::make_pair(SENS1_INT_GPIO_Port, SENS1_INT_Pin),
        std::make_pair(SENS1_RX_GPIO_Port, SENS1_RX_Pin),
        std::make_pair(SENS1_TX_GPIO_Port, SENS1_TX_Pin),
        std::make_pair(I2C2_SCL_GPIO_Port, I2C2_SCL_Pin),
        std::make_pair(I2C2_SDA_GPIO_Port, I2C2_SDA_Pin),
};
const std::array <std::pair<GPIO_TypeDef*, uint16_t>, 12> SENSOR2  {
        std::make_pair(SENS12_MOSI_GPIO_Port, SENS12_MOSI_Pin),
        std::make_pair(SENS12_MISO_GPIO_Port, SENS12_MISO_Pin),
        std::make_pair(SENS12_SCK_GPIO_Port, SENS12_SCK_Pin),
        std::make_pair(SENS2_CS_GPIO_Port, SENS2_CS_Pin),
        std::make_pair(SENS2_RST_GPIO_Port, SENS2_RST_Pin),
        std::make_pair(SENS2_ANALOG_GPIO_Port, SENS2_ANALOG_Pin),
        std::make_pair(SENS2_PWM_GPIO_Port, SENS2_PWM_Pin),
        std::make_pair(SENS2_INT_GPIO_Port, SENS2_INT_Pin),
        std::make_pair(SENS2_RX_GPIO_Port, SENS2_RX_Pin),
        std::make_pair(SENS2_TX_GPIO_Port, SENS2_TX_Pin),
        std::make_pair(I2C2_SCL_GPIO_Port, I2C2_SCL_Pin),
        std::make_pair(I2C2_SDA_GPIO_Port, I2C2_SDA_Pin),
};
const std::array <std::pair<GPIO_TypeDef*, uint16_t>, 12> SENSOR3  {
        std::make_pair(SENS34_MOSI_GPIO_Port, SENS34_MOSI_Pin),
        std::make_pair(SENS34_MISO_GPIO_Port, SENS34_MISO_Pin),
        std::make_pair(SENS34_SCK_GPIO_Port, SENS34_SCK_Pin),
        std::make_pair(SENS3_CS_GPIO_Port, SENS3_CS_Pin),
        std::make_pair(SENS3_RST_GPIO_Port, SENS3_RST_Pin),
        std::make_pair(SENS3_ANALOG_GPIO_Port, SENS3_ANALOG_Pin),
        std::make_pair(SENS3_PWM_GPIO_Port, SENS3_PWM_Pin),
        std::make_pair(SENS3_INT_GPIO_Port, SENS3_INT_Pin),
        std::make_pair(SENS3_RX_GPIO_Port, SENS3_RX_Pin),
        std::make_pair(SENS3_TX_GPIO_Port, SENS3_TX_Pin),
        std::make_pair(I2C2_SCL_GPIO_Port, I2C2_SCL_Pin),
        std::make_pair(I2C2_SDA_GPIO_Port, I2C2_SDA_Pin),
};
const std::array <std::pair<GPIO_TypeDef*, uint16_t>, 12> SENSOR4  {
        std::make_pair(SENS34_MOSI_GPIO_Port, SENS34_MOSI_Pin),
        std::make_pair(SENS34_MISO_GPIO_Port, SENS34_MISO_Pin),
        std::make_pair(SENS34_SCK_GPIO_Port, SENS34_SCK_Pin),
        std::make_pair(SENS4_CS_GPIO_Port, SENS4_CS_Pin),
        std::make_pair(SENS4_RST_GPIO_Port, SENS4_RST_Pin),
        std::make_pair(SENS4_ANALOG_GPIO_Port, SENS4_ANALOG_Pin),
        std::make_pair(SENS4_PWM_GPIO_Port, SENS4_PWM_Pin),
        std::make_pair(SENS4_INT_GPIO_Port, SENS4_INT_Pin),
        std::make_pair(SENS4_RX_GPIO_Port, SENS4_RX_Pin),
        std::make_pair(SENS4_TX_GPIO_Port, SENS4_TX_Pin),
        std::make_pair(I2C2_SCL_GPIO_Port, I2C2_SCL_Pin),
        std::make_pair(I2C2_SDA_GPIO_Port, I2C2_SDA_Pin),
};

void AllSensorsTest(const std::array <std::pair<GPIO_TypeDef*, uint16_t>, 36> &SENSOR){
    for(auto i:SENSOR){
        HAL_GPIO_WritePin(i.first, i.second, GPIO_PIN_SET);
        HAL_Delay(100);
    }
}

void SENSOR_Test(const std::array <std::pair<GPIO_TypeDef*, uint16_t>, 12> &SENSOR){
    for(auto i:SENSOR){
        HAL_GPIO_WritePin(i.first, i.second, GPIO_PIN_SET);
        HAL_Delay(100);
    }
    for(auto i:SENSOR){
        HAL_GPIO_WritePin(i.first, i.second, GPIO_PIN_RESET);
        HAL_Delay(100);
    }
}

void TestMessage(std::string peripheral, int sensorNumber){
    uint8_t data_buffer[40]= {0};
    auto print = sprintf(reinterpret_cast<char *>(data_buffer), " Testing %s %i \n",peripheral.c_str(), sensorNumber);
    HAL_UART_Transmit(&huart2, data_buffer, print,10);
}


int BT1PushCounter = 0;
int BT2PushCounter = 0;
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {

    if (GPIO_Pin == BT1_Pin) {
        BT1PushCounter++;
    }
    else if (GPIO_Pin == BT2_Pin) {
        BT2PushCounter++;
        TestMessage("LED",BT2PushCounter);
        if(BT2PushCounter == 1){
            HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
        }else if(BT2PushCounter == 2){
            HAL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin);
        }else if(BT2PushCounter == 3){
            BT2PushCounter = 0;
            HAL_GPIO_TogglePin(LED3_GPIO_Port, LED3_Pin);
        }
    } else {
        __NOP();
    }
}


extern "C"
int Main() {
    AllSensorsTest(SENSORS);

    while(true){
        if(BT1PushCounter == 1){
            SENSOR_Test(SENSOR1);
            TestMessage("SENSOR",BT1PushCounter);
        }else if(BT1PushCounter == 2){
            SENSOR_Test(SENSOR2);
            TestMessage("SENSOR",BT1PushCounter);
        }else if(BT1PushCounter == 3){
            SENSOR_Test(SENSOR3);
            TestMessage("SENSOR", BT1PushCounter);
        }else if(BT1PushCounter == 4){
            SENSOR_Test(SENSOR4);
            TestMessage("SENSOR",BT1PushCounter);
        }else if(BT1PushCounter > 4){
            BT1PushCounter = 1;
        }

    }
}

