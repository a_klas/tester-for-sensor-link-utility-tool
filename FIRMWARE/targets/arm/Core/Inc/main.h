/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define BT1_Pin GPIO_PIN_13
#define BT1_GPIO_Port GPIOC
#define BT1_EXTI_IRQn EXTI15_10_IRQn
#define SENS4_ANALOG_Pin GPIO_PIN_0
#define SENS4_ANALOG_GPIO_Port GPIOC
#define SENS1_ANALOG_Pin GPIO_PIN_1
#define SENS1_ANALOG_GPIO_Port GPIOC
#define SENS1_INT_Pin GPIO_PIN_2
#define SENS1_INT_GPIO_Port GPIOC
#define SENS1_RST_Pin GPIO_PIN_3
#define SENS1_RST_GPIO_Port GPIOC
#define SENS1_TX_Pin GPIO_PIN_0
#define SENS1_TX_GPIO_Port GPIOA
#define SENS1_RX_Pin GPIO_PIN_1
#define SENS1_RX_GPIO_Port GPIOA
#define DEBUG_TX_Pin GPIO_PIN_2
#define DEBUG_TX_GPIO_Port GPIOA
#define DEBUG_RX_Pin GPIO_PIN_3
#define DEBUG_RX_GPIO_Port GPIOA
#define SENS1_CS_Pin GPIO_PIN_4
#define SENS1_CS_GPIO_Port GPIOA
#define LED2_Pin GPIO_PIN_5
#define LED2_GPIO_Port GPIOA
#define SENS3_INT_Pin GPIO_PIN_6
#define SENS3_INT_GPIO_Port GPIOA
#define SENS4_INT_Pin GPIO_PIN_7
#define SENS4_INT_GPIO_Port GPIOA
#define SENS4_TX_Pin GPIO_PIN_4
#define SENS4_TX_GPIO_Port GPIOC
#define SENS4_RX_Pin GPIO_PIN_5
#define SENS4_RX_GPIO_Port GPIOC
#define SENS2_ANALOG_Pin GPIO_PIN_0
#define SENS2_ANALOG_GPIO_Port GPIOB
#define SENS3_ANALOG_Pin GPIO_PIN_1
#define SENS3_ANALOG_GPIO_Port GPIOB
#define SENS4_RST_Pin GPIO_PIN_2
#define SENS4_RST_GPIO_Port GPIOB
#define SENS34_SCK_Pin GPIO_PIN_10
#define SENS34_SCK_GPIO_Port GPIOB
#define I2C2_SDA_Pin GPIO_PIN_11
#define I2C2_SDA_GPIO_Port GPIOB
#define SENS3_CS_Pin GPIO_PIN_12
#define SENS3_CS_GPIO_Port GPIOB
#define I2C2_SCL_Pin GPIO_PIN_13
#define I2C2_SCL_GPIO_Port GPIOB
#define SENS34_MISO_Pin GPIO_PIN_14
#define SENS34_MISO_GPIO_Port GPIOB
#define SENS34_MOSI_Pin GPIO_PIN_15
#define SENS34_MOSI_GPIO_Port GPIOB
#define SENS3_RST_Pin GPIO_PIN_6
#define SENS3_RST_GPIO_Port GPIOC
#define SENS4_CS_Pin GPIO_PIN_7
#define SENS4_CS_GPIO_Port GPIOC
#define SENS2_RST_Pin GPIO_PIN_8
#define SENS2_RST_GPIO_Port GPIOC
#define SENS2_CS_Pin GPIO_PIN_9
#define SENS2_CS_GPIO_Port GPIOC
#define BT2_Pin GPIO_PIN_8
#define BT2_GPIO_Port GPIOA
#define BT2_EXTI_IRQn EXTI9_5_IRQn
#define SENS3_TX_Pin GPIO_PIN_9
#define SENS3_TX_GPIO_Port GPIOA
#define SENS3_RX_Pin GPIO_PIN_10
#define SENS3_RX_GPIO_Port GPIOA
#define LED1_Pin GPIO_PIN_11
#define LED1_GPIO_Port GPIOA
#define LED3_Pin GPIO_PIN_12
#define LED3_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define SENS2_INT_Pin GPIO_PIN_15
#define SENS2_INT_GPIO_Port GPIOA
#define SENS12_SCK_Pin GPIO_PIN_10
#define SENS12_SCK_GPIO_Port GPIOC
#define SENS12_MISO_Pin GPIO_PIN_11
#define SENS12_MISO_GPIO_Port GPIOC
#define SENS2_TX_Pin GPIO_PIN_12
#define SENS2_TX_GPIO_Port GPIOC
#define SENS2_RX_Pin GPIO_PIN_2
#define SENS2_RX_GPIO_Port GPIOD
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define SENS12_MOSI_Pin GPIO_PIN_5
#define SENS12_MOSI_GPIO_Port GPIOB
#define SENS4_PWM_Pin GPIO_PIN_6
#define SENS4_PWM_GPIO_Port GPIOB
#define SENS1_PWM_Pin GPIO_PIN_7
#define SENS1_PWM_GPIO_Port GPIOB
#define SENS2_PWM_Pin GPIO_PIN_8
#define SENS2_PWM_GPIO_Port GPIOB
#define SENS3_PWM_Pin GPIO_PIN_9
#define SENS3_PWM_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
